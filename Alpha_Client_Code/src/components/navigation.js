import React from 'react';
import {  Navbar,Nav, NavItem } from 'react-bootstrap';
import './custom.css';

export class Navigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    return (

      <Navbar>
        <Navbar.Header >
          <Navbar.Brand>
            <a href="#"><b>Estimates Portal</b></a>
          </Navbar.Brand>
        </Navbar.Header>
        <Nav>
          <NavItem eventKey={1} href="#"><i className="fa fa-line-chart"></i>  <b>Competitor Analysis</b></NavItem>
          <NavItem eventKey={2} href="#"><b><i className="fa fa-line-chart"></i> Comapany Data</b></NavItem>
          <NavItem eventKey={3} href="#"  ><b ><i className="fa fa-clock-o" style={{ color: 'white', size: '80%' }}></i> My Estimates</b></NavItem>
          <NavItem eventKey={4} href="#"><b><i className="fa fa-clock-o" style={{ color: 'white', size: '80%' }}></i> Inputs</b></NavItem>

        </Nav>
      </Navbar>
    );
  }
}